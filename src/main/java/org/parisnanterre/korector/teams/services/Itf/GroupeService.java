package org.parisnanterre.korector.teams.services.Itf;

import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GroupeService {
    List<Groupe>all();
    Groupe save(Groupe g);
    Groupe getgroupbyid(Long id)throws RecordNotFoundException;
    void delete(Long id) throws RecordNotFoundException;
    List<Groupe> getGroupeByUser(String user);

}