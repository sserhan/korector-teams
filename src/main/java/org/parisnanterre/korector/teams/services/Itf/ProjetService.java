package org.parisnanterre.korector.teams.services.Itf;

import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.entity.Projet;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjetService {
    List<Projet>all();
    Projet save(Projet g);
    Projet getOne(Long id)throws RecordNotFoundException;
    void delete(Long id) throws RecordNotFoundException;
    List<Projet> getProjetByGroupe(Groupe groupe);

}