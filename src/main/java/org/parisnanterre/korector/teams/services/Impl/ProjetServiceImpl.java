package org.parisnanterre.korector.teams.services.Impl;

import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.entity.Projet;
import org.parisnanterre.korector.teams.entity.Projet;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.parisnanterre.korector.teams.repository.ProjetRepository;
import org.parisnanterre.korector.teams.services.Itf.ProjetService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjetServiceImpl implements ProjetService {
    private final ProjetRepository projetRepository;

    public ProjetServiceImpl(ProjetRepository projetRepository) {
        this.projetRepository = projetRepository;
    }

    @Override
    public List<Projet> all(){
        return projetRepository.findAll();
    }

    @Override
    public Projet save(Projet g){
        g=projetRepository.save(g);
        return g;
    }

    @Override
    public Projet getOne(Long id) throws RecordNotFoundException{
        Optional<Projet> g=projetRepository.findById(id);
        if(g.isPresent()){
            return g.get();
        }
        else{
            throw new RecordNotFoundException("No group record exist for given id");

        }
    }

    @Override
    public void delete(Long id)  throws RecordNotFoundException{
        Optional<Projet> g=projetRepository.findById(id);
        if(g.isPresent()){
            projetRepository.deleteById(id);
        }
        else {
            throw new RecordNotFoundException("No group record exist for given id");
        }

    }

    @Override
    public List<Projet> getProjetByGroupe(Groupe groupe) {
        return projetRepository.getProjetByGroupe(groupe);
    }
}
