package org.parisnanterre.korector.teams.services.Impl;

import org.parisnanterre.korector.teams.services.Itf.GroupeService;
import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.parisnanterre.korector.teams.repository.GroupeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroupeServiceImpl implements GroupeService {


    private final GroupeRepository groupeRepository;

    public GroupeServiceImpl(GroupeRepository groupeRepository) {
        this.groupeRepository = groupeRepository;
    }

    @Override
    public List<Groupe> all(){
        return groupeRepository.findAll();
    }

    @Override
    public Groupe save(Groupe g){
        g=groupeRepository.save(g);
        return g;
    }

    @Override
    public Groupe getgroupbyid(Long id) throws RecordNotFoundException{
        Optional<Groupe> g=groupeRepository.findById(id);
        if(g.isPresent()){
            return g.get();
        }
        else{
            throw new RecordNotFoundException("No group record exist for given id");

        }
    }

    @Override
    public void delete(Long id)  throws RecordNotFoundException{
        Optional<Groupe> g=groupeRepository.findById(id);
        if(g.isPresent()){
            groupeRepository.deleteById(id);
        }
        else {
            throw new RecordNotFoundException("No group record exist for given id");
        }

    }

    @Override
    public List<Groupe> getGroupeByUser(String user) {
        return groupeRepository.getGroupesByUser(user);
    }

    /*@Override
    public Groupe update(Groupe g){
        /*Optional<Groupe> group=groupeRepository.findById(g.getId());
        Groupe newgroup=group.get();
        newgroup.setNom(g.getNom());
        newgroup.setUsers(g.getUsers());
        newgroup.setProjets(g.getProjets());
        newgroup.setDescription(g.getDescription());
        groupeRepository.save(g);
        return newgroup;
    }*/


}