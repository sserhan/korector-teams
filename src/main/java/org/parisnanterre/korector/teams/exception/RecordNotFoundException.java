package org.parisnanterre.korector.teams.exception;

public class RecordNotFoundException extends Exception{

    public RecordNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}