package org.parisnanterre.korector.teams.repository;

import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.entity.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Long> {
    List<Projet> getProjetByGroupe(Groupe groupe);
}
