package org.parisnanterre.korector.teams.repository;

import org.parisnanterre.korector.teams.entity.Groupe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupeRepository extends JpaRepository<Groupe, Long> {
    @Query("SELECT g FROM Groupe g JOIN g.users u where u = :user")
    List<Groupe> getGroupesByUser(@Param("user") String user);
}
