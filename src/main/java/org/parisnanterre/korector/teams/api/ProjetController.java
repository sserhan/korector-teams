package org.parisnanterre.korector.teams.api;

import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.parisnanterre.korector.teams.payload.ModelMapper;
import org.parisnanterre.korector.teams.payload.request.ProjetForm;
import org.parisnanterre.korector.teams.payload.response.ProjetResponse;
import org.parisnanterre.korector.teams.services.Itf.GroupeService;
import org.parisnanterre.korector.teams.services.Itf.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping(path="/api/v1")
public class ProjetController {
    @Autowired
    private ProjetService projetService;
    @Autowired
    private GroupeService groupeService;

    @RequestMapping(path = "/projets", method = RequestMethod.GET)
    public ResponseEntity<List<ProjetResponse>> getProjets() {
        List<ProjetResponse> projets = new ArrayList<>();
        projetService.all()
                .forEach(projet -> {
                    projets.add(ModelMapper.projetToProjetResponse(projet));
                });
        return new ResponseEntity<>(projets, HttpStatus.OK);
    }



    //Créer un projet
    @RequestMapping(path = "/projets", method = RequestMethod.POST)
    public ProjetResponse create(@RequestBody ProjetForm projetForm) throws RecordNotFoundException {
        return ModelMapper.projetToProjetResponse(
                projetService.save(
                        ModelMapper.projetFormToProjet(projetForm)
                ));
    }

    //Consulter un projet
    @RequestMapping(path = "/projets/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProjetResponse> getProjetById(@PathVariable Long id) throws RecordNotFoundException {
        return ResponseEntity.ok(
                ModelMapper.projetToProjetResponse(
                        projetService.getOne(id)
                ));
    }

    //Modifier projet
    @RequestMapping(path="/projets", method = RequestMethod.PUT)
    public ResponseEntity<ProjetResponse> updateProjet(@RequestBody ProjetForm projetForm) throws RecordNotFoundException {
        return ResponseEntity.ok(ModelMapper.projetToProjetResponse(
                projetService.save(
                        ModelMapper.projetFormToProjet(projetForm)
                ))
        );
    }

    //Supprimer un projet
    @RequestMapping(path="/projets/{id}",method = RequestMethod.DELETE)
    public void deleteProjet(@PathVariable Long id)  throws RecordNotFoundException {
        this.projetService.delete(id);
    }

    @GetMapping(path = "/projets/user/{userId}")
    public ResponseEntity<List<ProjetResponse>> getProjetsByUser(@PathVariable String userId){
        List<ProjetResponse> projetResponses = new ArrayList<>();
        groupeService
                .getGroupeByUser(userId)
                .forEach(groupe -> {
                    if(groupe.getUsers().contains(userId)){
                        groupe.getProjets()
                                .forEach(projet -> projetResponses.add(
                                        ModelMapper.projetToProjetResponse(projet)
                                ));
                    }
                });
        return new ResponseEntity<>(projetResponses, HttpStatus.OK);
    }

    @GetMapping(path = "/projets/group/{groupeId}")
    public ResponseEntity<List<ProjetResponse>> getProjetByGroupe(@PathVariable Long groupeId) throws RecordNotFoundException {
        List<ProjetResponse>projetResponses = new ArrayList<>();
        projetService
                .getProjetByGroupe(groupeService.getgroupbyid(groupeId))
                .forEach(projet -> projetResponses.add(
                        ModelMapper.projetToProjetResponse(projet)
                ));
        return new ResponseEntity<>(projetResponses, HttpStatus.OK);
    }
}
