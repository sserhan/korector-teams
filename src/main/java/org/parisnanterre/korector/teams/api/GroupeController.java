package org.parisnanterre.korector.teams.api;

import org.parisnanterre.korector.teams.payload.request.GroupeForm;
import org.parisnanterre.korector.teams.payload.response.GroupeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.parisnanterre.korector.teams.services.Itf.GroupeService;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.parisnanterre.korector.teams.payload.ModelMapper;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
public class GroupeController {

    @Autowired
    private GroupeService groupeService;

    //lister tout les groupes
    @RequestMapping(path = "/groupes", method = RequestMethod.GET)
    public ResponseEntity<List<GroupeResponse>> getGroupes() {
        List<GroupeResponse> groupes = new ArrayList<>();
        groupeService.all()
                .forEach(groupe -> {
                    groupes.add(ModelMapper.GroupeToGroupeResponse(groupe));
                });
        return new ResponseEntity<>(groupes, HttpStatus.OK);
    }

    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public String test() {
        return "OK de teams";
    }

    //Créer un groupe
    @RequestMapping(path = "/groups", method = RequestMethod.POST)
    public GroupeResponse create(@RequestBody GroupeForm groupeForm) {
        return ModelMapper.GroupeToGroupeResponse(
                groupeService.save(
                        ModelMapper.GroupeFormToGroupe(groupeForm)
                ));
    }

    //Consulter un groupe
    @RequestMapping(path = "/groups/{id}", method = RequestMethod.GET)
    public ResponseEntity<GroupeResponse> getGroupById(@PathVariable Long id) throws RecordNotFoundException {
        return ResponseEntity.ok(
                ModelMapper.GroupeToGroupeResponse(
                        groupeService.getgroupbyid(id)
                ));
    }

    //Modifier groupe
    @RequestMapping(path="/groups", method = RequestMethod.PUT)
    public ResponseEntity<GroupeResponse> updateGroup(@RequestBody GroupeForm groupeForm){
        return ResponseEntity.ok(ModelMapper.GroupeToGroupeResponse(
                groupeService.save(
                        ModelMapper.GroupeFormToGroupe(groupeForm)
                ))
        );
    }

    //Supprimer un groupe
    @RequestMapping(path="groups/{id}",method = RequestMethod.DELETE)
    public void deletegroup(@PathVariable Long id)  throws RecordNotFoundException {
        this.groupeService.delete(id);
    }

    @GetMapping(path = "/groups/user/{userId}")
    public ResponseEntity<List<GroupeResponse>> getGroupesbyUser(@PathVariable String userId){
        List<GroupeResponse> groupeResponses = new ArrayList<>();
        groupeService
                .getGroupeByUser(userId)
                .forEach(groupe -> groupeResponses.add(
                        ModelMapper.GroupeToGroupeResponse(groupe)
                ));
        return new ResponseEntity<>(groupeResponses, HttpStatus.OK);
    }
}
