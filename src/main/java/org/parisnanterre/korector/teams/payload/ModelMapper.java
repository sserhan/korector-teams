package org.parisnanterre.korector.teams.payload;
import org.parisnanterre.korector.teams.entity.Groupe;
import org.parisnanterre.korector.teams.entity.Projet;
import org.parisnanterre.korector.teams.exception.RecordNotFoundException;
import org.parisnanterre.korector.teams.payload.request.GroupeForm;
import org.parisnanterre.korector.teams.payload.request.ProjetForm;
import org.parisnanterre.korector.teams.payload.response.GroupeResponse;
import org.parisnanterre.korector.teams.payload.response.ProjetResponse;
import org.parisnanterre.korector.teams.services.Itf.GroupeService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ModelMapper {
    private static GroupeService groupeService;

    public ModelMapper(GroupeService groupeService) {
        ModelMapper.groupeService = groupeService;
    }

    public static GroupeResponse GroupeToGroupeResponse(Groupe groupe){
        Set<ProjetResponse> projets = new HashSet<>();
        GroupeResponse grp = new GroupeResponse();
        grp.setId(groupe.getId());
        grp.setNom(groupe.getNom());
        grp.setManagers(groupe.getManagers());
        grp.setUsers(groupe.getUsers());
        groupe.getProjets()
                .forEach(projet -> projets.add(
                        projetToProjetResponse(projet)
                ));
        grp.setProjets(projets);
        grp.setDescription(groupe.getDescription());
        return grp;
    }

    public static Groupe GroupeFormToGroupe(GroupeForm form){
        Groupe g = new Groupe();
        if(form.getId() != null) g.setId(form.getId());
        g.setNom(form.getNom());
        g.setManagers(form.getManagers());
        g.setUsers(form.getUsers());
        g.setDescription(form.getDescription());
        return g;
    }

    public static ProjetResponse projetToProjetResponse(Projet projet){
        ProjetResponse projetResponse = new ProjetResponse();
        projetResponse.setId(projet.getId());
        projetResponse.setGitProvider(projet.getGitProvider());
        projetResponse.setGitRepoName(projet.getGitRepoName());
        projetResponse.setNomGroupe(projet.getGroupe().getNom());
        projetResponse.setNomProjet(projet.getNomProjet());
        return projetResponse;
    }

    public static Projet projetFormToProjet(ProjetForm projetForm) throws RecordNotFoundException {
        Projet projet = new Projet();
        if(projetForm.getId() != null) projet.setId(projetForm.getId());
        projet.setNomProjet(projetForm.getNomProjet());
        projet.setGroupe(groupeService.getgroupbyid(projetForm.getGroupeId()));
        projet.setGitRepoName(projetForm.getGitRepoName());
        projet.setGitProvider(projetForm.getGitProvider());
        return projet;
    }








}

